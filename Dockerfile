# Build
FROM node:latest AS builder

RUN git clone https://github.com/vuejs/vue-router.git /vue-router

WORKDIR /vue-router

RUN npm install
RUN npm run docs:build

# Serve
FROM nginx:alpine

COPY --from=builder /vue-router/docs/.vuepress/dist /usr/share/nginx/html
